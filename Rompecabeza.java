import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;
import javax.swing.JOptionPane;
import java.io.*;

public class Rompecabeza implements ActionListener
{
   private JFrame ventana;
   private JButton[] btn_boton;
   private JButton btn_invisible, btn_iniciar, btn_iniciar2, btn_tmp, btn_jugadores;
   private JLabel lbl_presentacion, lbl_tiempo, lbl_intent;
   private JTextField tf_tiempo, tf_intent, tf_nombre;
   Timer conTmp;
   
   int varx=0, vary=0, indice, x, y, invX, invY, index;
   
   Timer timer = new Timer(50, new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int x = btn_boton[index].getLocation().x;
        int y = btn_boton[index].getLocation().y;
        if(x == invX && y == invY){
            System.out.println("continua... "+btn_tmp.getText()+ "x: "+ x + " i_x: "+invX +" y: "+y+" i_y:"+invY);
            timer.stop();
            if(verificar()){
                System.out.println("Ganaste");
                conTmp.stop();
                nombre();
            }
         }else{
            btn_boton[index].setLocation(x+varx,y+vary);
            System.out.println("entro timer x: "+x+" y: "+y);
         }
      }
    });
   
   JScrollPane listScroller;
   
   DefaultListModel<String> listModel;
   JList<String> lst_lista;
   
   public static void main(String[] args)
   {
      new Rompecabeza();

   }
   
   Rompecabeza()
   {
      
      ventana = new JFrame("Rompecabeza");
      ventana.setBounds(100,100,500,500);
      ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      ventana.setExtendedState(JFrame.MAXIMIZED_BOTH);
      ventana.setLayout(null);
      
      btn_boton = new JButton[15];
      for (int i=0; i<15; i++)
      {
         btn_boton[i] = new JButton(String.valueOf(i+1));
         btn_boton[i].setBounds(120+50*(i%4),75+20*(i/4),50,20);
         btn_boton[i].addActionListener(this);
         ventana.add(btn_boton[i]);
      }  
      
      btn_invisible = new JButton();
      btn_invisible.setBounds(120+50*(15%4), 75+20*(15/4), 50, 20);
      ventana.add(btn_invisible);
      btn_invisible.setVisible(false);     
      
       btn_iniciar = new JButton("INICIAR");
       btn_iniciar.setBounds(120,200,80,20);
       btn_iniciar.addActionListener(this);
       ventana.add(btn_iniciar);
       
       btn_iniciar2 = new JButton("INICIAR2");
       btn_iniciar2.setBounds(220,200,100,20);
       btn_iniciar2.addActionListener(this);
       ventana.add(btn_iniciar2);
       
       btn_jugadores = new JButton("TOP 5 JUGADORES");
       btn_jugadores.setBounds(150,250,150,20);
       btn_jugadores.addActionListener(this);
       ventana.add(btn_jugadores);       
      
       lbl_tiempo = new JLabel("TIEMPO: ");
       lbl_tiempo.setBounds(350,75,80,20);
       ventana.add(lbl_tiempo);
      
       lbl_intent = new JLabel("INTENTOS: ");
       lbl_intent.setBounds(350,100,120,20);
       ventana.add(lbl_intent);
     
       tf_tiempo = new JTextField("0");
       tf_tiempo.setBounds(400,75,40,20);
       ventana.add(tf_tiempo);
      
       tf_intent = new JTextField("0");
       tf_intent.setBounds(420,100,40,20);
       ventana.add(tf_intent);
      
       listModel = new DefaultListModel<String>();
       lst_lista = new JList<String>(listModel);
      
       listScroller = new JScrollPane(lst_lista);
       listScroller.setBounds(90,290,300,300);
       ventana.add(listScroller);
      
      ventana.setVisible(true);
   }
   
   public void actionPerformed(ActionEvent e)
   {

      if (e.getSource() == btn_iniciar)
         {
             revolver();
             tiempo();
             
         }
      else if (e.getSource() == btn_iniciar2)
         {
            contar();
            tiempo();
            invX = btn_invisible.getLocation().x;
            invY = btn_invisible.getLocation().y;
            btn_invisible.setLocation(btn_boton[14].getLocation().x,btn_boton[14].getLocation().y);
            btn_boton[14].setLocation(invX,invY);
         }
      else
        {
                  
          if (e.getSource() == btn_jugadores)
            {
             jugadores();
            }
             else 
              {
                if(!timer.isRunning())
                  {
                    btn_tmp = (JButton)e.getSource();
                    contar();
                    mover();                    
                  }
              }
        }
           
   }
   
    public void revolver()
        {
          System.out.println("entro a revolver");
          int i, j, x, y;
          Random rnd = new Random();
          for (i=0;i<15;i++)
          {
             j = rnd.nextInt(15);
             x = btn_boton[i].getLocation().x;
             y = btn_boton[i].getLocation().y;
             btn_boton[i].setLocation(btn_boton[j].getLocation().x,btn_boton[j].getLocation().y);
             btn_boton[j].setLocation(x,y);
          }
        }
    
    public void mover()
        { 
          System.out.println("entro a mover");
            
           int start = 0;
                varx = 0;
                vary = 0;
                
                x = btn_tmp.getLocation().x;
                y = btn_tmp.getLocation().y;
                invX = btn_invisible.getLocation().x;
                invY = btn_invisible.getLocation().y;
                System.out.println("num: "+btn_tmp.getText()+ "x: "+ x + " i_x: "+invX +" y: "+y+" i_y:"+invY);
                System.out.println(y == invY);
                System.out.println(x-invX);   
                   if(y == invY && (x-invX)*-1 == 50){
                    varx=10;
                    start = 1;
                    indice = Integer.parseInt(btn_tmp.getText());
                    btn_invisible.setLocation(x,y); 

                } else if(y == invY && (x-invX) == 50){
                    varx=-10;
                    start = 1;
                    indice = Integer.parseInt(btn_tmp.getText());
                    btn_invisible.setLocation(x,y); 

                }else if(x == invX && (y-invY) == 20){
                    vary = -10;
                    start = 1;
                    indice = Integer.parseInt(btn_tmp.getText());
                    btn_invisible.setLocation(x,y);

                }else if(x == invX && (y-invY)*-1 == 20){
                    vary=10;
                    start = 1;
                    indice = Integer.parseInt(btn_tmp.getText());
                    btn_invisible.setLocation(x,y);
                }
                if(start == 1){
                    index = indice -1;
                    timer.start();
                }   
    
          }
          
    public void contar()
      {
      System.out.println("entro a contar");
      int v, cMov=0;
      v = Integer.parseInt(tf_intent.getText());
      tf_intent.setText(String.valueOf(v+1));
      cMov = v +1;
      System.out.println("esta contando: "+cMov);
    
       }
    
    public void tiempo()
    {
       System.out.println("entro a tiempo");
       conTmp = new Timer (1000, new ActionListener()
              {
                 public void actionPerformed(ActionEvent e)
                   {
                     int t;
                     t = Integer.parseInt(tf_tiempo.getText());
                     tf_tiempo.setText(String.valueOf(t+1));
                   }
               });
               
        conTmp.start();
    
    }
    
    public boolean verificar()
    {
       System.out.println("entro a verificar");
       for(int i=0;i<15;i++)
       {
            int posX = btn_boton[i].getLocation().x;
            int posY = btn_boton[i].getLocation().y;
            int posInd = Integer.parseInt(btn_boton[i].getText());
            int calX = 120+50*(i%4);
            int calY = 75+20*(i/4);
            
         if(posX == calX && posY == calY && (posInd-1) == i)
            {

            }
        else
        {
            return false;
        }
        }
        return true;
    }
    
    public void nombre()
    {
      System.out.println("entro a nombre");
      String nombre = JOptionPane.showInputDialog("Escribe tu nombre");
      tf_nombre = new JTextField(nombre);
      
      FileWriter fw;
      try
      {
         fw = new FileWriter("jugadores con mejores puntos.txt",true);
         fw.write(tf_nombre.getText()+"\n ");
         
         fw.close();
      }
      catch(Exception e)
      {
         System.out.println("Error grabar "+e.toString());
      }
    }
    
    
    public void jugadores()
    {
      System.out.println("entro a jugadores");
      File f = new File("jugadores con mejores puntos.txt");
      Scanner sc;
      String jugador;
      int tiempo;
      try
      {
         sc = new Scanner(f);
         listModel.clear();
         while (sc.hasNextLine())
         {
            jugador = sc.nextLine();
            listModel.addElement(jugador);
         }
      }
      catch(Exception e)
      {
         System.out.println("Error leer "+e.toString());
      }
    }
}